import { useEffect, useState } from "react";



export default function Secure() {

    const [userDetails, setUserDetails] = useState({});
    const [adminToken, setAdminToken] = useState("");

    useEffect(() => {
        setUserDetails(parseJwt(localStorage.getItem("token")))
    }, []);

    function parseJwt(token) {
        try {
            return JSON.parse(atob(token.split('.')[1]));
        } catch (e) {
            return null;
        }
    };

    function logOut() {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

        var urlencoded = new URLSearchParams();
        urlencoded.append("username", "admin");
        urlencoded.append("password", "admin");
        urlencoded.append("grant_type", "password");
        urlencoded.append("client_id", "admin-cli");

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: urlencoded,
            redirect: 'follow'
        };

        fetch("https://keycloak-auth-service.herokuapp.com/auth/realms/master/protocol/openid-connect/token", requestOptions)
            .then(response => response.text())
            .then(result => console.log(result))
            .catch(error => console.log('error', error));
    }


    const User = (
        <div>
            <h1>Success! 😎</h1>

            <h4>Raw Token from Keycloak</h4>
            <div className="text-truncate">
                {userDetails && localStorage.getItem("token")}
            </div>
            <br />
            <h4>User details extracted from Token</h4>
            <div className="card">

                <div className="card-body">
                    <h4>Username</h4>
                    <p>{userDetails && userDetails.name}</p>

                    <h4>Email</h4>
                    <p>{userDetails && userDetails.email}</p>

                    <div>
                        <h4>Default Roles</h4>
                        <ul>
                            {userDetails.resource_access && userDetails.resource_access.account.roles.map((r, index) => (<li key={index}>{r}</li>))}
                        </ul>
                    </div>
                </div>
            </div>
            <br />
            <button type="button" onClick={logOut} className="btn btn-primary btn-block">Log out</button>
        </div>
    )



    return (
        <div>
            {User}
        </div>
    )
}